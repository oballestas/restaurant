from django.urls import path, include
from rest_framework import routers

from invoice.views import InvoiceListView, CreateInvoiceView, UpdateInvoiceView, DeleteInvoiceView, HomeView, \
    InvoiceViewSet

router = routers.DefaultRouter()
router.register(r'invoice_list', InvoiceViewSet)

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('invoices/', InvoiceListView.as_view(), name='invoices'),
    path('add_invoice/', CreateInvoiceView.as_view(), name='add_invoice'),
    path('update_invoice/<int:pk>/', UpdateInvoiceView.as_view(), name='update_invoice'),
    path('delete_invoice/<int:pk>/', DeleteInvoiceView.as_view(), name='delete_invoice'),
]
