# Create your views here.

# Waiter Views
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, TemplateView
from rest_framework import viewsets, permissions

from invoice.forms import InvoiceForm
from invoice.models import Invoice
from invoice.serializer import InvoiceSerializer


class HomeView(TemplateView):
    template_name = 'home.html'


class InvoiceListView(ListView):
    model = Invoice
    queryset = Invoice.objects.all()
    template_name = 'invoice/invoice_list.html'
    context_object_name = 'invoices'


class CreateInvoiceView(CreateView):
    model = Invoice
    template_name = 'invoice/create_invoice.html'
    form_class = InvoiceForm
    success_url = '/invoices/'


class UpdateInvoiceView(UpdateView):
    model = Invoice
    form_class = InvoiceForm
    pk_url_kwarg = 'pk'
    template_name = 'invoice/create_invoice.html'
    success_url = '/invoices/'


class DeleteInvoiceView(DeleteView):
    model = Invoice
    pk_url_kwarg = 'pk'
    template_name = 'order/delete_order.html'
    success_url = '/invoices/'


# REST Framework views

class InvoiceViewSet(viewsets.ModelViewSet):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer
    permission_classes = [permissions.IsAuthenticated]
