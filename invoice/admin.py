from django.contrib import admin

# Register your models here.
from invoice.models import Invoice


@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    list_display = ('id', 'client', 'waiter', 'date', 'order')
