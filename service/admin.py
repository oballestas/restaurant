from django.contrib import admin

# Register your models here.
from service.models import Table, ProductType, Product, Order, OrderItem

admin.site.register(Table)
admin.site.register(ProductType)
admin.site.register(Order)


@admin.register(OrderItem)
class OrderItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'product', 'quantity', 'order')


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'type', 'price')
