from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from rest_framework import viewsets, permissions
from rest_framework.permissions import IsAuthenticated

from service.models import Order, OrderItem, ProductType, Product, Table
from service.forms import OrderForm, OrderItemForm, ProductTypeForm, ProductForm, TableForm

# Orders Views
from service.serializer import OrderItemSerializer, ProductSerializer, OrderSerializer, ProductTypeSerializer, \
    TableSerializer


class CreateOrderView(CreateView):
    model = Order
    template_name = 'order/create_order.html'
    form_class = OrderForm
    success_url = '/orders/'


class UpdateOrderView(UpdateView):
    model = Order
    form_class = OrderForm
    pk_url_kwarg = 'pk'
    template_name = 'order/create_order.html'
    success_url = '/orders/'


class OrdersListView(ListView):
    model = Order
    queryset = Order.objects.all()
    template_name = 'order/order_list.html'
    context_object_name = 'orders'


class DeleteOrderView(DeleteView):
    model = Order
    pk_url_kwarg = 'pk'
    template_name = 'order/delete_order.html'
    success_url = '/orders/'


# Order Items Views

class CreateOrderItemView(CreateView):
    model = OrderItem
    template_name = 'order_items/create_order_item.html'
    form_class = OrderItemForm
    success_url = '/order_items/'


class UpdateOrderItemView(UpdateView):
    model = OrderItem
    form_class = OrderItemForm
    pk_url_kwarg = 'pk'
    template_name = 'order_items/create_order_item.html'
    success_url = '/order_items/'


class OrderItemsListView(ListView):
    model = OrderItem
    queryset = OrderItem.objects.all()
    template_name = 'order_items/order_items_list.html'
    context_object_name = 'orders_items'


class DeleteOrderItemView(DeleteView):
    model = OrderItem
    pk_url_kwarg = 'pk'
    template_name = 'order/delete_order.html'
    success_url = '/order_items/'


# Product Type Views

class CreateProductTypeView(CreateView):
    model = ProductType
    template_name = 'product_type/create_product_type.html'
    form_class = ProductTypeForm
    success_url = '/product_types/'


class UpdateProductTypView(UpdateView):
    model = ProductType
    form_class = ProductTypeForm
    pk_url_kwarg = 'pk'
    template_name = 'product_type/create_product_type.html'
    success_url = '/product_types/'


class ProductTypeListView(ListView):
    model = ProductType
    queryset = ProductType.objects.all()
    template_name = 'product_type/product_type_list.html'
    context_object_name = 'product_types'


class DeleteProductTypeView(DeleteView):
    model = ProductType
    pk_url_kwarg = 'pk'
    template_name = 'order/delete_order.html'
    success_url = '/product_types/'


# Product Type Views

class ProductListView(ListView):
    model = Product
    queryset = Product.objects.all()
    template_name = 'product/product_list.html'
    context_object_name = 'products'


class CreateProductView(CreateView):
    model = Product
    template_name = 'product/create_product.html'
    form_class = ProductForm
    success_url = '/products/'


class UpdateProductView(UpdateView):
    model = Product
    form_class = ProductForm
    pk_url_kwarg = 'pk'
    template_name = 'product/create_product.html'
    success_url = '/products/'


class DeleteProductView(DeleteView):
    model = Product
    pk_url_kwarg = 'pk'
    template_name = 'order/delete_order.html'
    success_url = '/products/'


# Table Views
class TableListView(ListView):
    model = Table
    queryset = Table.objects.all()
    template_name = 'table/table_list.html'
    context_object_name = 'tables'


class CreateTableView(CreateView):
    model = Table
    template_name = 'table/create_table.html'
    form_class = TableForm
    success_url = '/tables/'


class UpdateTableView(UpdateView):
    model = Table
    form_class = TableForm
    pk_url_kwarg = 'pk'
    template_name = 'table/create_table.html'
    success_url = '/tables/'


class DeleteTableView(DeleteView):
    model = Table
    pk_url_kwarg = 'pk'
    template_name = 'order/delete_order.html'
    success_url = '/tables/'


# REST Framework views

class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [permissions.IsAuthenticated]


class OrderItemViewSet(viewsets.ModelViewSet):
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer
    permission_classes = [permissions.IsAuthenticated]


class ProductTypeViewSet(viewsets.ModelViewSet):
    queryset = ProductType.objects.all()
    serializer_class = ProductTypeSerializer
    permission_classes = [permissions.IsAuthenticated]


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [permissions.IsAuthenticated]


class TableViewSet(viewsets.ModelViewSet):
    queryset = Table.objects.all()
    serializer_class = TableSerializer
    permission_classes = [permissions.IsAuthenticated]
