from django.db import models


# Create your models here.
class Table(models.Model):
    diners_num = models.IntegerField(null=False)
    location = models.CharField(max_length=45, null=False, unique=True)

    def __str__(self):
        return self.location


class ProductType(models.Model):
    type = models.CharField(max_length=30)

    def __str__(self):
        return self.type


class Product(models.Model):
    name = models.CharField(max_length=30)
    type = models.ForeignKey(ProductType, on_delete=models.CASCADE)
    price = models.FloatField()

    def __str__(self):
        return self.name


class Order(models.Model):
    reference = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.reference


class OrderItem(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    order = models.ForeignKey(Order, on_delete=models.CASCADE)

    def __str__(self):
        return self.product.name
