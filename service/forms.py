from django import forms
from django.forms import TextInput

from service.models import Order, OrderItem, ProductType, Product, Table


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = '__all__'

        widgets = {
            'reference': TextInput(attrs={
                'class': "form-control mb-3",
                'style': 'max-width: 500px;',
            }),
        }


class OrderItemForm(forms.ModelForm):
    class Meta:
        model = OrderItem
        fields = '__all__'


class ProductTypeForm(forms.ModelForm):
    class Meta:
        model = ProductType
        fields = '__all__'

        widgets = {
            'type': TextInput(attrs={
                'class': "form-control mb-3",
                'style': 'max-width: 500px;',
            }),
        }


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = '__all__'


class TableForm(forms.ModelForm):
    class Meta:
        model = Table
        fields = '__all__'
