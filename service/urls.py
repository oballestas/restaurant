from django.urls import path, include
from rest_framework import routers

from service.views import OrdersListView, CreateOrderView, UpdateOrderView, DeleteOrderView, OrderItemsListView, \
    CreateOrderItemView, UpdateOrderItemView, DeleteOrderItemView, ProductTypeListView, CreateProductTypeView, \
    UpdateProductTypView, DeleteProductTypeView, ProductListView, CreateProductView, UpdateProductView, \
    DeleteProductView, TableListView, CreateTableView, UpdateTableView, DeleteTableView, OrderItemViewSet, \
    ProductViewSet, OrderViewSet, ProductTypeViewSet, TableViewSet

router = routers.DefaultRouter()
router.register(r'order', OrderViewSet)
router.register(r'order_item', OrderItemViewSet)
router.register(r'product_type', ProductTypeViewSet)
router.register(r'product', ProductViewSet)
router.register(r'table', TableViewSet)

urlpatterns = [
    path('orders/', OrdersListView.as_view(), name='orders'),
    path('add_order/', CreateOrderView.as_view(), name='add_order'),
    path('update_order/<int:pk>/', UpdateOrderView.as_view(), name='update_order'),
    path('delete_order/<int:pk>/', DeleteOrderView.as_view(), name='delete_order'),
    path('order_items/', OrderItemsListView.as_view(), name='order_items'),
    path('add_order_item/', CreateOrderItemView.as_view(), name='add_order_item'),
    path('update_order_item/<int:pk>/', UpdateOrderItemView.as_view(), name='update_order_item'),
    path('delete_order_item/<int:pk>/', DeleteOrderItemView.as_view(), name='delete_order_item'),
    path('product_types/', ProductTypeListView.as_view(), name='product_types'),
    path('add_product_type/', CreateProductTypeView.as_view(), name='add_product_type'),
    path('update_product_type/<int:pk>/', UpdateProductTypView.as_view(), name='update_product_type'),
    path('delete_product_type/<int:pk>/', DeleteProductTypeView.as_view(), name='delete_product_type'),
    path('products/', ProductListView.as_view(), name='products'),
    path('add_product/', CreateProductView.as_view(), name='add_product'),
    path('update_product/<int:pk>/', UpdateProductView.as_view(), name='update_product'),
    path('delete_product/<int:pk>/', DeleteProductView.as_view(), name='delete_product'),
    path('tables/', TableListView.as_view(), name='tables'),
    path('add_table/', CreateTableView.as_view(), name='add_table'),
    path('update_table/<int:pk>/', UpdateTableView.as_view(), name='update_table'),
    path('delete_table/<int:pk>/', DeleteTableView.as_view(), name='delete_table'),
]
