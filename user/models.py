from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Client(models.Model):
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    observations = models.CharField(max_length=200)

    def __str__(self):
        return f'{self.name} {self.last_name}'


class Waiter(models.Model):
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    last_name2 = models.CharField(max_length=50)

    def __str__(self):
        return f'{self.name} {self.last_name}'
