from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from rest_framework import viewsets, permissions

from user.forms import ClientForm, WaiterForm
from user.models import Client, Waiter

# Client Views
from user.serializer import ClientSerializer, WaiterSerializer


class ClientListView(ListView):
    model = Client
    queryset = Client.objects.all()
    template_name = 'client/client_list.html'
    context_object_name = 'clients'


class CreateClientView(CreateView):
    model = Client
    template_name = 'client/create_client.html'
    form_class = ClientForm
    success_url = '/clients/'


class UpdateClientView(UpdateView):
    model = Client
    form_class = ClientForm
    pk_url_kwarg = 'pk'
    template_name = 'client/create_client.html'
    success_url = '/clients/'


class DeleteClientView(DeleteView):
    model = Client
    pk_url_kwarg = 'pk'
    template_name = 'order/delete_order.html'
    success_url = '/clients/'


# Waiter Views
class WaiterListView(ListView):
    model = Waiter
    queryset = Waiter.objects.all()
    template_name = 'waiter/waiter_list.html'
    context_object_name = 'waiters'


class CreateWaiterView(CreateView):
    model = Waiter
    template_name = 'waiter/create_waiter.html'
    form_class = WaiterForm
    success_url = '/waiters/'


class UpdateWaiterView(UpdateView):
    model = Waiter
    form_class = WaiterForm
    pk_url_kwarg = 'pk'
    template_name = 'waiter/create_waiter.html'
    success_url = '/waiters/'


class DeleteWaiterView(DeleteView):
    model = Waiter
    pk_url_kwarg = 'pk'
    template_name = 'order/delete_order.html'
    success_url = '/waiters/'


# REST Framework views

class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = [permissions.IsAuthenticated]


class WaiterViewSet(viewsets.ModelViewSet):
    queryset = Waiter.objects.all()
    serializer_class = WaiterSerializer
    permission_classes = [permissions.IsAuthenticated]
