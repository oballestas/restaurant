from django.contrib import admin

# Register your models here.
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from user.models import Client, Waiter

admin.site.register(Client)
admin.site.register(Waiter)
# class ClientInline(admin.StackedInline):
#     model = Client
#     # can_delete = False
#     verbose_name_plural = 'client'
#
#
# class WaiterInline(admin.StackedInline):
#     model = Waiter
#     # can_delete = False
#     verbose_name_plural = 'waiter'
#
#
# # Define a new User admin
# class UserAdmin(UserAdmin):
#     inlines = (ClientInline, WaiterInline)
#
#
# # Re-register UserAdmin
# admin.site.unregister(User)
# admin.site.register(User, UserAdmin)
