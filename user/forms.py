from django import forms

from user.models import Client, Waiter


class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = '__all__'


class WaiterForm(forms.ModelForm):
    class Meta:
        model = Waiter
        fields = '__all__'
