from django.urls import path, include
from rest_framework import routers

from user.views import ClientListView, CreateClientView, UpdateClientView, DeleteClientView, WaiterListView, \
    CreateWaiterView, UpdateWaiterView, DeleteWaiterView, ClientViewSet, WaiterViewSet

router = routers.DefaultRouter()
router.register(r'client', ClientViewSet)
router.register(r'waiter', WaiterViewSet)

urlpatterns = [
    path('clients/', ClientListView.as_view(), name='clients'),
    path('add_client/', CreateClientView.as_view(), name='add_client'),
    path('update_client/<int:pk>/', UpdateClientView.as_view(), name='update_client'),
    path('delete_client/<int:pk>/', DeleteClientView.as_view(), name='delete_client'),
    path('waiters/', WaiterListView.as_view(), name='waiters'),
    path('add_waiter/', CreateWaiterView.as_view(), name='add_waiter'),
    path('update_waiter/<int:pk>/', UpdateWaiterView.as_view(), name='update_waiter'),
    path('delete_waiter/<int:pk>/', DeleteWaiterView.as_view(), name='delete_waiter'),
]
